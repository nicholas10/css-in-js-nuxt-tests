# CSS-in-JS Tests

> Styled Components and Shared CSS and JS Variables Nuxt Test

## You can look at this

https://focused-brahmagupta-3b44ee.netlify.com/

## You can run this project

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```
